from rest_framework.test import APITestCase, APIClient


class BaseTest(APITestCase):

    def setUp(self):
        
        self.admin_user = {
            "username":"superadmin",
            "password":"superadminpassword",
            "email":"supper@admin.com"
        }
        self.registration_data = {
                "username":"walimike",
                "password":"password",
                "email":"walimike@email.com",
                "password2":"password"
            }
        self.second_user = {
                "password2": "Users@12345",
                "username": "user1",
                "email": "userstest1@gmail.com",
                "password": "Users@12345"
            }
        self.third_user_data = {
                "password2": "Users@12345",
                "username": "user3",
                "email": "userstest3@gmail.com",
                "password": "Users@12345"
            }
        self.invalid_user_data = {
                "username": "user3",
                "email": "userstest3@gmail.com",
                "password": "Users@12345"
            }
        self.auth_url = '/api/v1/auth/'

    def post_data(self, url, data):
        response = self.client.post(url, data, format='json')
        return response
