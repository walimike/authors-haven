from rest_framework import status
from api.apps.authentication.models import User

from .test_base import BaseTest


class AuthenticationTest(BaseTest):

    def test_valid_user_registration(self):
        response = self.post_data(f"{self.auth_url}signup/", self.registration_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        assert 'access' in response.data

    def test_invalid_user_registration(self):
        response = self.post_data(f"{self.auth_url}signup/", self.invalid_user_data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("This field is required.", str(response.data))

    def test_valid_login(self):
        self.post_data(f"{self.auth_url}signup/", self.registration_data)
        response = self.post_data(f"{self.auth_url}login/", self.registration_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.get().email, "walimike@email.com")
        assert 'access' in response.data

    def test_login_with_non_existing_user(self):
        self.post_data(f"{self.auth_url}signup/", self.registration_data)
        response = self.post_data(f"{self.auth_url}login/", self.second_user)
        self.assertEqual(response.status_code, 401)
        self.assertIn("No active account found with the given credentials", str(response.data))
